﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Vucity.Modules.VSCOptix.Util
{
    class SceneModel
    {
        public Stack<int> IndicesStack = new Stack<int>();
        public int[] Indices
        {
            get { return IndicesStack.ToArray(); }
        }
        

        public Stack<Vector3> VerticesStack = new Stack<Vector3>();
        public Vector3[] Vertices
        {
            get { return VerticesStack.ToArray(); }
        }

        List<int> Buildings = new List<int>();
        
        /// <summary>
        /// Add geometry of a MeshFilter if not already known
        /// </summary>
        /// <param name="meshFilter"></param>
        public void AddMeshFilter(MeshFilter meshFilter)
        {
            if (Buildings.Contains(meshFilter.GetInstanceID()))
            {
                return;
            }
            var startIndex = Vertices.Length;
            foreach (var sharedMeshVertex in meshFilter.mesh.vertices)
            {
                var pos = meshFilter.gameObject.transform.TransformPoint(sharedMeshVertex);

                VerticesStack.Push(new Vector3(pos.x, pos.y, pos.z));
            }

            foreach (var triangle in meshFilter.mesh.GetIndices(0))
            {
                IndicesStack.Push(startIndex + triangle);
            }
            var vertices = Vertices;
            var modVertStack = new Stack<Vector3>();
            foreach (var vert in vertices)
            {
                modVertStack.Push(new Vector3(vert.x, vert.y, vert.z));
            }

            VerticesStack = modVertStack;
            Buildings.Add(meshFilter.GetInstanceID());
        }

        public void Reset()
        {
            Buildings = new List<int>();
            VerticesStack = new Stack<Vector3>();
            IndicesStack = new Stack<int>();
        }
    }
}
