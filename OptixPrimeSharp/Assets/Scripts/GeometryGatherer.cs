﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Vucity.Modules.VSCOptix
{
    public class GeometryGatherer : MonoBehaviour
    {
        public List<MeshFilter> MeshFilters = new List<MeshFilter>();
        public GameObject Target;
        public void Gather()
        {
            var inSceneFilters = GameObject.FindObjectsOfType<MeshFilter>();
            var filters = new Stack<MeshFilter>();
            foreach (var curFilter in inSceneFilters)
            {
                //if (curFilter.gameObject.tag == TileStatics.Tags.Buildings && (Target == null || Target != curFilter.gameObject))
                //{
                    filters.Push(curFilter);
                //}
            }

            MeshFilters = filters.ToList();
        }
    }
}