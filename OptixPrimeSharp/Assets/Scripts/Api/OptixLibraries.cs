﻿using System;
using System.IO;
using UnityEngine;

namespace Assets.Vucity.Modules.VSCOptix.Api
{
    internal class OptixLibraries
    {
        public const string OptixLib = "optix.6.0.0.dll";//"optix.1.dll";
        public const string OptixPrimeLib = "optix_prime.6.0.0";
        public const string OptixULib = "optixu.1.dll";
        static OptixLibraries() // static Constructor
        {
            var currentPath = Environment.GetEnvironmentVariable("PATH",
                EnvironmentVariableTarget.Process);
#if UNITY_EDITOR_32
    var dllPath = Application.dataPath
        + Path.DirectorySeparatorChar + "SomePath"
        + Path.DirectorySeparatorChar + "Plugins"
        + Path.DirectorySeparatorChar + "x86";
#elif UNITY_EDITOR_64
            var dllPath = Application.dataPath
                          //+ Path.DirectorySeparatorChar + "SomePath"
                          + Path.DirectorySeparatorChar + "Plugins"
                          + Path.DirectorySeparatorChar + "x86_64";
#else // Player
    var dllPath = Application.dataPath
        + Path.DirectorySeparatorChar + "Plugins";

#endif
            Debug.Log(dllPath);
            if (currentPath != null && currentPath.Contains(dllPath) == false)
                Environment.SetEnvironmentVariable("PATH", currentPath + Path.PathSeparator
                                                                       + dllPath, EnvironmentVariableTarget.Process);
        }

    }
}
