﻿using UnityEngine;

namespace Assets.Vucity.Modules.VSCOptix
{
    [RequireComponent(typeof(OptixPrimeScanner))]
    [RequireComponent(typeof(GeometryGatherer))]
    public class OptixPrimeInput : MonoBehaviour
    {
        private OptixPrimeScanner _optixPrimeScanner;
        private GeometryGatherer _geometryGatherer;
        private bool _runCalculations;

        void Start()
        {
            _optixPrimeScanner = GetComponent<OptixPrimeScanner>();
            _geometryGatherer = GetComponent<GeometryGatherer>();
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                Debug.Log("Resetting");
                _optixPrimeScanner.ResetModel();
            }

            if (Input.GetKeyDown(KeyCode.P))
            {
                Debug.Log("Gathering and processing scene geometry");
                _geometryGatherer.Gather();
                Debug.Log("Gathered");
                _optixPrimeScanner.AddMeshFiltersToModel();
                Debug.Log("Added");
            }

            if (Input.GetKeyDown(KeyCode.O))
            {
                Debug.Log("Recalculating rays");
                _optixPrimeScanner.CalculateRays();
                Debug.Log("Calced");
            }

            if (Input.GetKeyDown(KeyCode.C))
            {
                _runCalculations = !_runCalculations;
                Debug.Log("Toggling calculations " + _runCalculations);
            }

            if (_runCalculations)
            {
                _optixPrimeScanner.Run();
            }
        }
    }
}
