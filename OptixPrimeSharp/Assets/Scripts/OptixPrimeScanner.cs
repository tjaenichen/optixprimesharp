﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Assets.Vucity.Modules.VSCOptix.Api;
using Assets.Vucity.Modules.VSCOptix.Util;
using UnityEngine;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;

namespace Assets.Vucity.Modules.VSCOptix
{
    [RequireComponent(typeof(GeometryGatherer))]
    class OptixPrimeScanner : MonoBehaviour
    {
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        struct Ray
        {
            public Vector3 origin;
            public float tmin;
            public Vector3 dir;
            public float tmax;
        };

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        struct RayOriginDirection
        {
            public Vector3 origin;
            public Vector3 dir;
        }
   
        public Transform Origin;
        public List<GameObject> Targets;
        public int MaxRays;
        private GeometryGatherer _geometryGatherer;
        SceneModel _sceneModel = new SceneModel();
        private RayOriginDirection[] _rays;

        void Start()
        {
            _geometryGatherer = GetComponent<GeometryGatherer>();
        }

        void Update()
        {
            //if (_geometryGatherer.MeshFilters != null && _geometryGatherer.MeshFilters.Count > 0)
            //{
            //    Run();
            //}
        }
        
        IntPtr InjectModel(IntPtr context)
        {
            var indices = _sceneModel.Indices;
            var vIndicesHandle = GCHandle.Alloc(indices, GCHandleType.Pinned);

            IntPtr indicesDesc;

            CheckError(OptixPrime.rtpBufferDescCreate(
                    context,
                    RTPbufferformat.RTP_BUFFER_FORMAT_INDICES_INT3,
                    RTPbuffertype.RTP_BUFFER_TYPE_HOST,
                    vIndicesHandle.AddrOfPinnedObject(),
                    out indicesDesc), "Create vIndex Buffer"
            );
            //Debug.Log(model.Indices.Length);
            ulong numIndices = (ulong)(indices.Length / 3);
            CheckError(OptixPrime.rtpBufferDescSetRange(indicesDesc, 0, numIndices), "Set vert index Buffer range");


            var newVertStack = new Stack<Vector3>();

            var vertices = _sceneModel.Vertices;

            for (var index = 0; index < vertices.Length; index++)
            {
                newVertStack.Push(new Vector3(vertices[index].x, vertices[index].y, vertices[index].z));
            }
            
            _sceneModel.VerticesStack = newVertStack;

            var verticesBufferPointer = GCHandle.Alloc(vertices, GCHandleType.Pinned);
            IntPtr verticesDesc;

            CheckError(OptixPrime.rtpBufferDescCreate(
                context,
                RTPbufferformat.RTP_BUFFER_FORMAT_VERTEX_FLOAT3,
                RTPbuffertype.RTP_BUFFER_TYPE_HOST,
                verticesBufferPointer.AddrOfPinnedObject(),
                out verticesDesc), "Create vertices Buffer");
            ulong size = (ulong)vertices.Length;
            CheckError(OptixPrime.rtpBufferDescSetRange(verticesDesc, 0, size), "Set vert buffer range");

            //
            // Create the Model object
            //
            IntPtr modelPtr;
            CheckError(OptixPrime.rtpModelCreate(context, out modelPtr), "Model create");
            CheckError(OptixPrime.rtpModelSetTriangles(modelPtr, indicesDesc, verticesDesc), "Set Triangles");
            CheckError(OptixPrime.rtpModelUpdate(modelPtr, 0), "Update Model");
            return modelPtr;
        }

        IntPtr Setup()
        {
            IntPtr context;
            uint device = 0;

            OptixPrime.rtpContextCreate(RTPcontexttype.RTP_CONTEXT_TYPE_CUDA, out context);

            OptixPrime.rtpContextSetCudaDeviceNumbers(context, 1, device);
            return context;
        }

        public void Run()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            IntPtr context = Setup();
            
            var modelPtr = InjectModel(context);

            //
            // Create buffer for ray input 
            //
           
            var raysBufferPointer = GCHandle.Alloc(_rays, GCHandleType.Pinned);

            IntPtr raysDesc;
            CheckError(OptixPrime.rtpBufferDescCreate(
                context,
                RTPbufferformat.RTP_BUFFER_FORMAT_RAY_ORIGIN_DIRECTION, /*RTP_BUFFER_FORMAT_RAY_ORIGIN_TMIN_DIRECTION_TMAX*/
                RTPbuffertype.RTP_BUFFER_TYPE_HOST, // raysBuffer.type(),
                raysBufferPointer.AddrOfPinnedObject(),
                out raysDesc), "Create Rays buffer");
            ulong raysCount = (ulong) _rays.Length;
            CheckError(OptixPrime.rtpBufferDescSetRange(raysDesc, 0, raysCount), "Set rays buffer length");

            //
            // Create buffer for returned hit descriptions
            //
            
            var hitsList = new float[MaxRays];
           
            var hitsArrayBuifferPointer = GCHandle.Alloc(hitsList, GCHandleType.Pinned);
            IntPtr hitsDesc;
            CheckError(OptixPrime.rtpBufferDescCreate(
                    context,
                    RTPbufferformat.RTP_BUFFER_FORMAT_HIT_T, 
                    RTPbuffertype.RTP_BUFFER_TYPE_HOST,
                    hitsArrayBuifferPointer.AddrOfPinnedObject(),
                    out hitsDesc), "Create Hits buffer"
            );
            ulong hitsCount = (ulong)hitsList.Length;
            CheckError(OptixPrime.rtpBufferDescSetRange(hitsDesc, 0, hitsCount), "Set hits buffer length");

            ////
            //// Execute query
            ////
            IntPtr query;
            CheckError(OptixPrime.rtpQueryCreate(modelPtr, 
                    RTPquerytype.RTP_QUERY_TYPE_CLOSEST, 
                    out query),"Create query");
            CheckError(OptixPrime.rtpQuerySetRays(query, raysDesc), "Set rays");
            CheckError(OptixPrime.rtpQuerySetHits(query, hitsDesc), "Set hits");
            //Debug.Log(stopwatch.Elapsed);
            stopwatch.Restart();
            CheckError(OptixPrime.rtpQueryExecute(query, 0), "Execute query");
            Debug.Log("Ran calc " + _rays.Length  + " rays against ");
            Debug.Log(_geometryGatherer.MeshFilters.Count + " buildings");
            Debug.Log((_sceneModel.VerticesStack.Count / 3) + " triangles");
            Debug.Log(stopwatch.Elapsed);

            for (var index = 0; index < hitsList.Length; index++)
            {
                var hit = hitsList[index];
                if (hit > 0)
                {
                    var dir = _rays[index].dir;
                    Debug.DrawRay( _rays[index].origin,  new Vector3(dir.x, dir.y, dir.z) * hit, Color.blue, .1f);
                }
                else
                {
                    var dir = _rays[index].dir;
                    //Debug.DrawRay(Origin.position, new Vector3(dir.x, dir.y, dir.z) * hit.Distance, Color.red, .1f);
                    //Debug.DrawRay(raysArray[index].origin, new Vector3(dir.x, dir.y, dir.z) * hit.Distance, Color.red, .1f);
                }
            }

            ////
            //// cleanup
            ////
            OptixPrime.rtpContextDestroy(context);
            Console.ReadKey();
        }

        public void CalculateRays()
        {
            var raysList = new Stack<RayOriginDirection>();

            for (int i = 0; i < MaxRays; i++)
            {
                var ray = new RayOriginDirection();
                ray.origin = new Vector3(Origin.position.x, Origin.position.y, Origin.position.z);
                ray.dir = new Vector3(Random.insideUnitSphere.x, Random.insideUnitSphere.y, Random.insideUnitSphere.z);

                raysList.Push(ray);
            }

            _rays = raysList.ToArray();
        }

        struct HitsWithId
        {
            public float Distance;
            public int Id;
        }
        public static void MarshalUnmananagedArray2Struct<T>(IntPtr unmanagedArray, int length, out T[] mangagedArray)
        {
            var size = Marshal.SizeOf(typeof(T));
            mangagedArray = new T[length];

            for (int i = 0; i < length; i++)
            {
                IntPtr ins = new IntPtr(unmanagedArray.ToInt64() + i * size);
                mangagedArray[i] = Marshal.PtrToStructure<T>(ins);
            }
        }
        static void CheckError(RTPresult result, string what = "")
        {
            if (result != RTPresult.RTP_SUCCESS)
            {
                string message;
                OptixPrime.rtpGetErrorString(result, out message);
                Debug.Log(message);
//                Console.ReadKey();
                throw new Exception($"Optix context error : {message}");
            }
            else if (!string.IsNullOrEmpty(what))
            {
                //Debug.Log("Ok: " + what);
            }
        }

        public void ResetModel()
        {
            _sceneModel.Reset();
        }

        public void AddMeshFiltersToModel()
        {
            if (_geometryGatherer.MeshFilters == null)
            {
                return;
            }
            foreach (var o in _geometryGatherer.MeshFilters)
            {
                _sceneModel.AddMeshFilter(o);
            }
        }

    }
}
